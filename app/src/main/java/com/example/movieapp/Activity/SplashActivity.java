package com.example.movieapp.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.movieapp.R;

public class SplashActivity extends Activity implements Animation.AnimationListener {

        Animation animRotate,animFadein;
        ImageView imageView;
        TextView title;

@Override
public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        imageView = (ImageView) findViewById(R.id.imageView);
        title = (TextView) findViewById(R.id.title);

        animRotate = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoom_in);
        animRotate.setAnimationListener(this);
        imageView.startAnimation(animRotate);

        animFadein = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_in);
        animFadein.setAnimationListener(this);
        title.startAnimation(animFadein);

        Thread splashThread = new Thread() {
@Override
public void run() {


        try {

        sleep(5000);
        } catch (InterruptedException e) {

        } finally {
                Intent i = new Intent(SplashActivity.this, NavigationDrawerActivity.class);
                startActivity(i);
                finish();
        }
        }
        };
        splashThread.start();
        }

@Override
public void onAnimationStart(Animation animation) {

        }

@Override
public void onAnimationEnd(Animation animation) {

        }

@Override
public void onAnimationRepeat(Animation animation) {

        }
        }