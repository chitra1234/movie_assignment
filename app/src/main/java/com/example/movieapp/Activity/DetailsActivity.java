package com.example.movieapp.Activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.movieapp.APIClient;
import com.example.movieapp.APIInterface;
import com.example.movieapp.R;
import com.example.movieapp.pojo.DetailsMovies;
import com.example.movieapp.pojo.Rating;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.movieapp.frafment.HomePage.API_KEY;

public class DetailsActivity extends AppCompatActivity {

   APIInterface apiInterface;
   ImageView bannerImageView;
   TextView release_date,title,description,genre;
   int movie_id;
   String genre_ = "";
   RatingBar rating;
   ProgressDialog progressDialog;


   @Override
   protected void onCreate(@Nullable Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       setContentView(R.layout.layout_details);

       bannerImageView = findViewById(R.id.bannerImageView);
       release_date = findViewById(R.id.release_date);
       title = findViewById(R.id.title);
       description = findViewById(R.id.description);
       genre = findViewById(R.id.genre);
       rating = findViewById(R.id.rating);

       // showing loader here
       progressDialog = new ProgressDialog(DetailsActivity.this);
       progressDialog.setMessage("Loading...");
       progressDialog.show();

       movie_id = getIntent().getIntExtra("movie_id",0);

       apiInterface = APIClient.getClient().create(APIInterface.class);

       Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
       setSupportActionBar(toolbar);
       getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       toolbar.setNavigationOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               onBackPressed();
           }
       });

       /*Author:Chitra
       * @desc: As no rating value is coming from api here I am showing dommy rating bar but after clicking on the ratingbar
       * you can rate the perticular movie*/

     rating.setOnTouchListener(new View.OnTouchListener() {
         @SuppressLint("ClickableViewAccessibility")
         @Override
         public boolean onTouch(View v, MotionEvent event) {
             final Dialog dialog = new Dialog(DetailsActivity.this);
             dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
             dialog.setCancelable(false);
             dialog.setContentView(R.layout.dialog_rating);

             RatingBar rating = dialog.findViewById(R.id.ratingbar);
             Button submit = dialog.findViewById(R.id.submit);

             submit.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {

                     String rating_value =String.valueOf(rating.getRating());

                     Call<Rating> call = apiInterface.doGetMovieRatings(movie_id,rating_value,"828e37b7f18dea883fcf84b2d85b06e1e4585f9d",API_KEY);
                     call.enqueue(new Callback<Rating>() {
                         @Override
                         public void onResponse(Call<Rating> call, Response<Rating> response) {
                             Log.d("TAG",response.message()+"");
                             Rating res = response.body();

                             boolean code = res.success;
                             if(code) {
                                 Toast.makeText(DetailsActivity.this,  "Thank you for your rating.", Toast.LENGTH_SHORT).show();
                             } else {
                                 Toast.makeText(DetailsActivity.this,  res.statusMessage, Toast.LENGTH_SHORT).show();
                             }
                         }

                         @Override
                         public void onFailure(Call<Rating> call, Throwable t) {

                         }
                     });


                     dialog.dismiss();
                 }
             });

             dialog.setCancelable(true);
             dialog.show();

             return false;
         }
     });


     /*Here calling api for geting the movie details*/
       Call<DetailsMovies> call = apiInterface.doGetMovieDetails(movie_id,API_KEY,"en-US");
       call.enqueue(new Callback<DetailsMovies>() {
           @Override
           public void onResponse(Call<DetailsMovies> call, Response<DetailsMovies> response) {
               Log.d("TAG",response.code()+"");

               if(progressDialog != null & progressDialog.isShowing())
                   progressDialog.dismiss();

               DetailsMovies resource = response.body();

               String image = "https://image.tmdb.org/t/p/w185"+resource.backdropPath;

               if (image != null) {
                Picasso.with(DetailsActivity.this).load(image).error(R.drawable.no_image).placeholder(R.drawable.no_image).into(bannerImageView);
               }
               release_date.setText(resource.releaseDate);
               title.setText(resource.originalTitle);
               toolbar.setTitle(resource.originalTitle);
               description.setText(resource.overview);

               /*Below code is for collecting all the genres in a single string*/
               List<DetailsMovies.Genre> data = resource.genres;
               for(int i = 0; i <data.size(); i++){
                   String x = data.get(i).name;
                   genre_ = genre_ + (x) + ", ";
               }
               genre.setText(genre_);


           }

           @Override
           public void onFailure(Call<DetailsMovies> call, Throwable t) {

           }
       });

   }
}
