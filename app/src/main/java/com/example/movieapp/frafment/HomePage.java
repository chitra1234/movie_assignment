package com.example.movieapp.frafment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.example.movieapp.APIClient;
import com.example.movieapp.APIInterface;
import com.example.movieapp.R;
import com.example.movieapp.Util.EndlessRecyclerOnScrollListener;
import com.example.movieapp.Util.NetworkUtil;
import com.example.movieapp.adapter.HomeAdapter;
import com.example.movieapp.pojo.PopularMovies;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomePage extends Fragment implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener{

    RecyclerView recyclerview,banner_recycler_view;
    ProgressDialog progressDialog;
    HomeAdapter homeAdapter;
    private SliderLayout mDemoSlider;
    APIInterface apiInterface;
    List<PopularMovies.Result> totalll = new ArrayList<>();
    public static String API_KEY = "c698507bcfb80efc16d05712c4d8d9f6";
    int page_no = 1;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.home, container, false);

        recyclerview = rootView.findViewById(R.id.recycler_view);
        mDemoSlider = rootView.findViewById(R.id.slider);


        boolean isNetwork = NetworkUtil.getConnectivityStatusString(getActivity());

        // below code for banner
        HashMap<String,Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("Hannibal",R.drawable.banner1);
        file_maps.put("Big Bang Theory",R.drawable.banner2);
        file_maps.put("House of Cards",R.drawable.banner3);
        file_maps.put("Game of Thrones", R.drawable.banner4);

        for(String name : file_maps.keySet()){
            TextSliderView textSliderView = new TextSliderView(getActivity());
            // initialize a SliderLayout
            textSliderView
                    .image(file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);
            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setDuration(4000);
        mDemoSlider.addOnPageChangeListener(this);

        // showing loader here
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.show();
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        recyclerview.setLayoutManager(gridLayoutManager);

        if(isNetwork) {
            getMovieList(page_no);
        } else {
            if(progressDialog != null)
            progressDialog.dismiss();
            Toast.makeText(getActivity(), "No network connection", Toast.LENGTH_SHORT).show();
        }


        /*Author:Chitra
        * @desc: The below code is for pagination*/
        recyclerview.addOnScrollListener(new EndlessRecyclerOnScrollListener(gridLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                if (isNetwork){
                    getMovieList(current_page);
                } else {
                    if(progressDialog != null)
                        progressDialog.dismiss();
                    Toast.makeText(getActivity(), "No network connection", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return rootView;
    }

    // get all movie list here
    public void getMovieList(int page_no){
        apiInterface = APIClient.getClient().create(APIInterface.class);

        Call<PopularMovies> call = apiInterface.doGetMovieList(API_KEY,"en", page_no);
        call.enqueue(new Callback<PopularMovies>() {
            @Override
            public void onResponse(Call<PopularMovies> call, Response<PopularMovies> response) {

                if(response.code() == 200) {
                    if(progressDialog != null & progressDialog.isShowing())
                        progressDialog.dismiss();

                    PopularMovies resource = response.body();

                    int page = resource.page;
                    int total_page = resource.totalPages;
                    int total_result = resource.totalResults;

                    List<PopularMovies.Result> list = resource.results;
                    totalll.addAll(list);

                    if(page_no == 1) {
                        homeAdapter = new HomeAdapter( getContext(), totalll);
                        recyclerview.setAdapter(homeAdapter);
                    } else {
                        homeAdapter.setDataSet(totalll);
                    }

                } else {
                    if(progressDialog != null & progressDialog.isShowing())
                        progressDialog.dismiss();
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<PopularMovies> call, Throwable t) {

            }
        });

    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}