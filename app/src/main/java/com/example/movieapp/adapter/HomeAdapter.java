package com.example.movieapp.adapter;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.RecyclerView;

import com.example.movieapp.Activity.DetailsActivity;
import com.example.movieapp.R;
import com.example.movieapp.pojo.PopularMovies;
import com.squareup.picasso.Picasso;
import java.util.List;


public class HomeAdapter  extends RecyclerView.Adapter<HomeAdapter.MyViewHolder> {

    Context context;
    List<PopularMovies.Result> list;

    public HomeAdapter(Context context, List<PopularMovies.Result> totalll) {
        this.context = context;
        this.list = totalll;
    }

    public void setDataSet(List<PopularMovies.Result> newDataSet) {
        this.list = newDataSet;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view1 = LayoutInflater.from(context).inflate(R.layout.homeadapter, parent, false);
        MyViewHolder holder = new MyViewHolder(view1);
        return holder;
    }
    @Override
    public void onBindViewHolder(final HomeAdapter.MyViewHolder holder, final int position) {

        String image = "https://image.tmdb.org/t/p/w185"+list.get(position).posterPath;

        if (image != null) {
            Picasso.with(context).load(image).error(R.drawable.no_image).placeholder(R.drawable.no_image).into(holder.module_photo);
        }

        holder.category.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailsActivity.class);
                intent.putExtra("movie_id",list.get(position).id);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount()  {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView module_photo;
        LinearLayout category;


        public MyViewHolder(View view1) {
            super(view1);

            module_photo = view1.findViewById(R.id.cat_photo);
            category = view1.findViewById(R.id.category);

        }
    }
}
