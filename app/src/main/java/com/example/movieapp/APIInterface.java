package com.example.movieapp;

import com.example.movieapp.pojo.DetailsMovies;
import com.example.movieapp.pojo.PopularMovies;
import com.example.movieapp.pojo.Rating;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIInterface {

   // Get all movie list api
   @GET("movie/popular")
   Call<PopularMovies> doGetMovieList(@Query("api_key") String api_key, @Query("language") String language,
                                      @Query("page") int page);

   // movie details api
   @GET("movie/{id}")
   Call<DetailsMovies> doGetMovieDetails(@Path("id") int id, @Query("api_key") String api_key, @Query("language") String language);

   // Rating APi
   @POST("movie/{id}/rating")
   Call<Rating> doGetMovieRatings(@Path("id") int id, @Query("value") String value, @Query("session_id") String session, @Query("api_key") String api_key);

}
