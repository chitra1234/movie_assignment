
package com.example.movieapp.pojo;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PopularMovies {

    @SerializedName("page")
    @Expose
    public Integer page;
    @SerializedName("results")
    @Expose
    public List<Result> results = null;
    @SerializedName("total_pages")
    @Expose
    public Integer totalPages;
    @SerializedName("total_results")
    @Expose
    public Integer totalResults;

    public List<Result> data = new ArrayList();

    public class Result {

        @SerializedName("adult")
        @Expose
        public Boolean adult;
        @SerializedName("backdrop_path")
        @Expose
        public String backdropPath;
        @SerializedName("genre_ids")
        @Expose
        public List<Integer> genreIds = null;
        @SerializedName("id")
        @Expose
        public Integer id;
        @SerializedName("original_language")
        @Expose
        public String originalLanguage;
        @SerializedName("original_title")
        @Expose
        public String originalTitle;
        @SerializedName("overview")
        @Expose
        public String overview;
        @SerializedName("popularity")
        @Expose
        public Double popularity;
        @SerializedName("poster_path")
        @Expose
        public String posterPath;
        @SerializedName("release_date")
        @Expose
        public String releaseDate;
        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("video")
        @Expose
        public Boolean video;
        @SerializedName("vote_average")
        @Expose
        public Double voteAverage;
        @SerializedName("vote_count")
        @Expose
        public Integer voteCount;

    }

}
